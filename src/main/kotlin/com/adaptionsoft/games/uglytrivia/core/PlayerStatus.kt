package com.adaptionsoft.games.uglytrivia.core

class PlayerStatus private constructor(
    val isOnPenaltyBox: Boolean,
    val isGettingOut: Boolean
) {
    val canMove = !isOnPenaltyBox || isGettingOut
    val canAnswer = !isOnPenaltyBox || isGettingOut

    fun evalRoll(roll: Roll) = if (isOnPenaltyBox) {
        val isOdd = roll.value % 2 != 0
        if (isOdd) GETTING_OUT else PENALIZED
    } else NO_PENALIZATION

    companion object {
        val NO_PENALIZATION =
            PlayerStatus(false, isGettingOut = false)
        val PENALIZED =
            PlayerStatus(true, isGettingOut = false)
        val GETTING_OUT =
            PlayerStatus(true, isGettingOut = true)
    }
}
