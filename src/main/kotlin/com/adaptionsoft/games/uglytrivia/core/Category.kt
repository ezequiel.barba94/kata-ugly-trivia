package com.adaptionsoft.games.uglytrivia.core

enum class Category(val label: String) {
    POP("Pop"),
    ROCK("Rock"),
    SCIENCE("Science"),
    SPORTS("Sports");

}