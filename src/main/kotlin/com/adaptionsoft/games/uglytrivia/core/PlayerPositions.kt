package com.adaptionsoft.games.uglytrivia.core

interface PlayerPositions {
    fun playersPlaying(): Int
    fun save(player: Player, place: Int)
    fun currentPosition(player: Player): Int?
}