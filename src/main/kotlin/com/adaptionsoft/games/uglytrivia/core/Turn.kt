package com.adaptionsoft.games.uglytrivia.core

class Turn {
    val players = mutableListOf<Player>()
    private var currentTurn = 0

    fun addPlayer(player: Player) {
        players += player
    }

    fun currentPlayer(): Player {
        return players[currentTurn]
    }

    fun nextTurn() {
        currentTurn = (currentTurn + 1) % players.size
    }
}