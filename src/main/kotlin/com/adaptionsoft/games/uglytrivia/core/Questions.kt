package com.adaptionsoft.games.uglytrivia.core

interface Questions {
    fun fromCategory(category: Category): Question
    fun askQuestion(category: Category): String
}