package com.adaptionsoft.games.uglytrivia.core

import com.adaptionsoft.games.uglytrivia.core.events.EventOne

class Rules(private val penaltyBox: PenaltyBox) {
    val sentToPenaltyBox = EventOne<Player>()
    val gettingOutPenaltyBox = EventOne<Player>()
    val notGettingOutPenaltyBox = EventOne<Player>()

    fun penalize(player: Player) {
        penaltyBox.put(player, PlayerStatus.PENALIZED)
        sentToPenaltyBox.raise(player)
    }

    fun isPenalized(player: Player): Boolean {
        return penaltyBox.findPlayerStatus(player).isOnPenaltyBox
    }

    fun evaluate(player: Player, roll: Roll): PlayerStatus {
        val status = penaltyBox.findPlayerStatus(player)
        val currentPenalization = status.evalRoll(roll)
        print(currentPenalization, player)
        penaltyBox.put(player, currentPenalization)
        return currentPenalization
    }

    private fun print(playerStatus: PlayerStatus, player: Player) {
        if (playerStatus.isOnPenaltyBox) {
            if (playerStatus.isGettingOut)
                gettingOutPenaltyBox.raise(player)
            else notGettingOutPenaltyBox.raise(player)
        }
    }

    fun canAnswer(player: Player): Boolean {
        val status = penaltyBox.findPlayerStatus(player)
        return status.canAnswer
    }

    fun canMove(player: Player): Boolean {
        val status = penaltyBox.findPlayerStatus(player)
        return status.canMove
    }
}