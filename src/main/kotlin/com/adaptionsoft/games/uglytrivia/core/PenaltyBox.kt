package com.adaptionsoft.games.uglytrivia.core

interface PenaltyBox {
    fun put(player: Player, penalty: PlayerStatus)
    fun findPlayerStatus(player: Player): PlayerStatus
}