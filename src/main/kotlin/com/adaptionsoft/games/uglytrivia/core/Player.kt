package com.adaptionsoft.games.uglytrivia.core

class Player(val name: String) {
    var coins = 0
        private set

    override fun toString() = name

    fun addCoin() {
        coins++
    }

    fun didWin(): Boolean {
        return coins == 6
    }
}