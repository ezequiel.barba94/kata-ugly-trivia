package com.adaptionsoft.games.uglytrivia.core.events

class EventTwo<T1, T2> {
    private val functions = mutableSetOf<(T1, T2) -> Unit>()

    fun raise(param1: T1, param2: T2) {
        for (function in functions) function(param1, param2)
    }

    operator fun plusAssign(function: (T1, T2) -> Unit) {
        functions += function
    }
}