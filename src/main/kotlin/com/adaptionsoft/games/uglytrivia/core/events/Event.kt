package com.adaptionsoft.games.uglytrivia.core.events

class Event {
    private val functions = mutableSetOf<() -> Unit>()

    fun raise() {
        for (function in functions) function()
    }

    operator fun plusAssign(function: () -> Unit) {
        functions += function
    }
}

