package com.adaptionsoft.games.uglytrivia.core

import com.adaptionsoft.games.uglytrivia.core.events.EventOne
import com.adaptionsoft.games.uglytrivia.core.events.EventTwo

class GameBoard(
    private val questions: Questions,
    private val playersPosition: PlayerPositions
) {
    private val currentlyPlaying get() = playersPosition.playersPlaying()

    val playerMovementEvent = EventTwo<Player, Int>()
    val categoryChangeEvent = EventOne<Category>()
    val playerAddedEvent = EventTwo<Player, Int>()

    fun move(player: Player, roll: Roll) {
        val place = advance(player, roll)
        playersPosition.save(player, place)
        playerMovementEvent.raise(player, place)
        val category = categoryOf(player)
        categoryChangeEvent.raise(category)
    }

    private fun advance(player: Player, roll: Roll): Int {
        return (playersPosition.currentPosition(player)!! + roll.value) % TOTAL_BOARD_POSITIONS
    }

    fun positionOf(player: Player): Int {
        return playersPosition.currentPosition(player)!!
    }

    fun addOnStartingPlace(player: Player) {
        if (playersPosition.playersPlaying() == MAX_PLAYERS) throw Exception()
        playersPosition.save(
            player,
            STARTING_POSITION
        )
        playerAddedEvent.raise(player, currentlyPlaying)
    }


    fun fetchQuestionFor(player: Player): Question {
        val category = categoryOf(player)
        return questions.fromCategory(category)
    }

    private fun categoryOf(player: Player): Category {
        return when (positionOf(player)) {
            0, 4, 8 -> Category.POP
            1, 5, 9 -> Category.SCIENCE
            2, 6, 10 -> Category.SPORTS
            3, 7, 11 -> Category.ROCK
            else -> throw Exception()
        }
    }

    companion object {
        private const val STARTING_POSITION = 0
        private const val MAX_PLAYERS = 5
        private const val TOTAL_BOARD_POSITIONS = 12
    }
}
