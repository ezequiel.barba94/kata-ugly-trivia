package com.adaptionsoft.games.uglytrivia.core.events

class EventOne<T> {
    private val functions = mutableSetOf<(T) -> Unit>()

    fun raise(param: T) {
        for (function in functions) function(param)
    }

    operator fun plusAssign(function: (T) -> Unit) {
        functions += function
    }
}
