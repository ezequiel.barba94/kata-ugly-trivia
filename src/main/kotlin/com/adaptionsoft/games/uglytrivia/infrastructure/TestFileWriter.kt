package com.adaptionsoft.games.uglytrivia.infrastructure

import com.google.gson.Gson
import java.io.OutputStreamWriter

class TestFileWriter {
    fun write(recorder: BoundedNumberRecorder) {
        Gson().newJsonWriter(OutputStreamWriter(System.out)).use {
            it.beginObject()
            it.name("untilFive")
            it.beginArray()
            for (num in recorder.recordingsOf(5)) it.value(num)
            it.endArray()
            it.name("untilNine")
            it.beginArray()
            for (num in recorder.recordingsOf(9)) it.value(num)
            it.endArray()
            it.endObject()
        }
    }
}