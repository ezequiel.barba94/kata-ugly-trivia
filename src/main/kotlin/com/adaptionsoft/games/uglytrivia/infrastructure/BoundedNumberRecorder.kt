package com.adaptionsoft.games.uglytrivia.infrastructure

import java.util.*


class BoundedNumberRecorder(private val random: Random) :
    BoundedRandom {
    private val recordings: MutableMap<Int, MutableList<Int>> = mutableMapOf()

    override fun nextInt(bound: Int): Int {
        val number = random.nextInt(bound)
        recordings[bound]?.add(number)
        return number
    }

    fun recordBounds(vararg bounds: Int) {
        for (bound in bounds)
            recordings.putIfAbsent(bound, mutableListOf())
    }

    fun recordingsOf(bound: Int): List<Int> {
        return recordings[bound] ?: listOf()
    }
}