package com.adaptionsoft.games.uglytrivia.infrastructure

import com.adaptionsoft.games.uglytrivia.core.Category
import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.Question
import com.adaptionsoft.games.uglytrivia.core.Roll
import java.io.PrintStream

class StreamEventWriter(private val stream: PrintStream) {
    fun correctAnswer(player: Player) {
        p("Answer was correct!!!!")
        p("${player.name} now has ${player.coins} Gold Coins.")
    }

    fun wrongAnswer() = p("Question was incorrectly answered")
    fun playerLocationChange(player: Player, place: Int) = p("$player's new location is $place")
    fun categoryChange(category: Category) = p("The category is ${category.label}")
    fun sentToPenaltyBox(player: Player) = p("$player was sent to the penalty box")
    fun gettingOutPenaltyBox(player: Player) = p("$player is getting out of the penalty box")
    fun notGettingOutPenaltyBox(player: Player) = p("$player is not getting out of the penalty box")
    fun askQuestion(question: Question) = p(question.question)

    fun playerAdded(player: Player, currentlyPlaying: Int) {
        p("$player was added")
        p("They are player number $currentlyPlaying")
    }

    fun turnStart(player: Player, roll: Roll) {
        p("$player is the current player")
        p("They have rolled a ${roll.value}")
    }

    private fun p(message: String) = stream.println(message)
}