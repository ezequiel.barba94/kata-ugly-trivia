package com.adaptionsoft.games.uglytrivia.infrastructure

interface BoundedRandom {
    fun nextInt(bound: Int): Int
}