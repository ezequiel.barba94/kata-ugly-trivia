package com.adaptionsoft.games.uglytrivia.infrastructure

import com.adaptionsoft.games.uglytrivia.core.PenaltyBox
import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.PlayerStatus

class InMemoryPenaltyBox : PenaltyBox {
    private val penalizedPlayers = mutableMapOf<Player, PlayerStatus>()

    override fun put(player: Player, penalty: PlayerStatus) {
        penalizedPlayers[player] = penalty
    }

    override fun findPlayerStatus(player: Player): PlayerStatus {
        return penalizedPlayers[player] ?: PlayerStatus.NO_PENALIZATION
    }

}