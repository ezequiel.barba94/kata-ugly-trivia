package com.adaptionsoft.games.uglytrivia.infrastructure

import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.PlayerPositions

class InMemoryPlayerPositions : PlayerPositions {
    private val playerPositions = mutableMapOf<Player, Int>()

    override fun playersPlaying() = playerPositions.size

    override fun save(player: Player, place: Int) {
        playerPositions[player] = place
    }

    override fun currentPosition(player: Player) = playerPositions[player]
}