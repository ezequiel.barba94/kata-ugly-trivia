package com.adaptionsoft.games.uglytrivia.actions

import com.adaptionsoft.games.uglytrivia.core.Rules
import com.adaptionsoft.games.uglytrivia.core.Turn
import com.adaptionsoft.games.uglytrivia.core.events.Event

class WrongAnswer(
    private val turn: Turn,
    private val rules: Rules
) {
    val wrongAnswerEvent = Event()

    operator fun invoke() {
        wrongAnswerEvent.raise()
        rules.penalize(turn.currentPlayer())
        turn.nextTurn()
    }
}
