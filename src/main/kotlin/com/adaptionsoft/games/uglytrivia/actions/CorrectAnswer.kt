package com.adaptionsoft.games.uglytrivia.actions

import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.Rules
import com.adaptionsoft.games.uglytrivia.core.Turn
import com.adaptionsoft.games.uglytrivia.core.events.EventOne

class CorrectAnswer(private val turn: Turn, private val rules: Rules) {
    val correctAnswerEvent = EventOne<Player>()

    operator fun invoke() {
        val player = turn.currentPlayer()
        if (rules.canAnswer(player))
            correctAnswer()
        turn.nextTurn()
    }

    private fun correctAnswer() {
        val player = turn.currentPlayer()
        player.addCoin()
        correctAnswerEvent.raise(player)
    }

}