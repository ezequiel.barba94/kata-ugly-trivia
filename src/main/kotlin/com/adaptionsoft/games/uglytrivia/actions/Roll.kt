package com.adaptionsoft.games.uglytrivia.actions

import com.adaptionsoft.games.uglytrivia.core.*
import com.adaptionsoft.games.uglytrivia.core.Roll
import com.adaptionsoft.games.uglytrivia.core.events.EventOne
import com.adaptionsoft.games.uglytrivia.core.events.EventTwo
import com.adaptionsoft.games.uglytrivia.core.Roll as RouletteRoll

class Roll(
    private val gameBoard: GameBoard,
    private val rules: Rules,
    private val turn: Turn
) {
    val turnStartEvent = EventTwo<Player, RouletteRoll>()
    val askQuestion = EventOne<Question>()

    operator fun invoke(roll: Int) {
        val player = turn.currentPlayer()
        val diceRoll = Roll(roll)
        turnStartEvent.raise(player, diceRoll)
        val penalization = rules.evaluate(player, diceRoll)
        if (penalization.canMove) {
            gameBoard.move(player, diceRoll)
            val question = gameBoard.fetchQuestionFor(player)
            askQuestion.raise(question)
        }
    }

}