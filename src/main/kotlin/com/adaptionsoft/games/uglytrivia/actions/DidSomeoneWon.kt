package com.adaptionsoft.games.uglytrivia.actions

import com.adaptionsoft.games.uglytrivia.core.Turn

class DidSomeoneWon(private val turn: Turn) {
    operator fun invoke() = turn.players.any { it.didWin() }
}