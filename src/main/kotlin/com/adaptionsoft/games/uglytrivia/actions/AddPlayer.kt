package com.adaptionsoft.games.uglytrivia.actions

import com.adaptionsoft.games.uglytrivia.core.GameBoard
import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.Turn

class AddPlayer(
    private val gameBoard: GameBoard,
    private val turn: Turn
) {
    operator fun invoke(playerName: String) {
        val player = Player(playerName)
        gameBoard.addOnStartingPlace(player)
        turn.addPlayer(player)
    }
}
