package com.adaptionsoft.games

import java.util.*

class MockedRandom : Random() {
    private val numbers = ArrayDeque<Int>()

    fun saveNextValues(vararg values: Int) {
        for (value in values) numbers += value
    }

    override fun nextInt(bound: Int): Int {
        return numbers.pollFirst()
    }
}