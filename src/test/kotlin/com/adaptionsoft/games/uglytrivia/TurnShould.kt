package com.adaptionsoft.games.uglytrivia

import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.Turn
import org.amshove.kluent.`should be`
import org.junit.jupiter.api.Test

class TurnShould {
    @Test
    fun `cycle between players`() {
        val turn = Turn()
        val playerOne = Player("one")
        val playerTwo = Player("two")
        val playerThree = Player("three")
        turn.addPlayer(playerOne)
        turn.addPlayer(playerTwo)
        turn.addPlayer(playerThree)

        turn.currentPlayer() `should be` playerOne
        turn.nextTurn()
        turn.currentPlayer() `should be` playerTwo
        turn.nextTurn()
        turn.currentPlayer() `should be` playerThree
        turn.nextTurn()
        turn.currentPlayer() `should be` playerOne
    }
}
