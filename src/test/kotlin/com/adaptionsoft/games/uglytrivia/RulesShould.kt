package com.adaptionsoft.games.uglytrivia

import com.adaptionsoft.games.uglytrivia.core.Player
import com.adaptionsoft.games.uglytrivia.core.Roll
import com.adaptionsoft.games.uglytrivia.core.Rules
import com.adaptionsoft.games.uglytrivia.infrastructure.InMemoryPenaltyBox
import org.amshove.kluent.`should be`
import org.junit.jupiter.api.Test

internal class RulesShould {
    @Test
    fun `penalize players`() {
        val player = Player("player")

        rules.penalize(player)

        rules.isPenalized(player) `should be` true
    }

    @Test
    fun `not allow the player answer when is penalized`() {
        val player = Player("player")

        rules.penalize(player)

        rules.canAnswer(player) `should be` false
    }

    @Test
    fun `allow the player answer when is it not penalized`() {
        val player = Player("player")

        rules.canAnswer(player) `should be` true
    }

    @Test
    fun `allow a penalized player move when the roll is odd`() {
        val player = Player("player")
        rules.penalize(player)

        rules.evaluate(player, Roll(3))
        val canMove = rules.canMove(player)

        canMove `should be` true
    }

    @Test
    fun `allow the player answer when is getting despenalized`() {
        val player = Player("player")
        rules.penalize(player)
        rules.evaluate(player, Roll(3))

        val canAnswer = rules.canAnswer(player)

        canAnswer `should be` true
    }

    private val rules = Rules(InMemoryPenaltyBox())
}