package com.adaptionsoft.games

import com.adaptionsoft.games.uglytrivia.infrastructure.BoundedNumberRecorder
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class BoundedNumberRecorderShould {
    @Test
    fun `record specified bounded randoms`() {
        val random = MockedRandom()
        random.saveNextValues(1, 2, 3, 4)
        val recorder = BoundedNumberRecorder(random)
        recorder.recordBounds(5, 9)

        recorder.nextInt(5)
        recorder.nextInt(7)
        recorder.nextInt(9)
        recorder.nextInt(5)

        Assertions.assertThat(recorder.recordingsOf(5)).containsExactly(1, 4)
        Assertions.assertThat(recorder.recordingsOf(7)).isEmpty()
        Assertions.assertThat(recorder.recordingsOf(9)).containsExactly(3)
    }
}