package com.adaptionsoft.games.trivia.runner

import com.adaptionsoft.games.uglytrivia.infrastructure.BoundedRandom
import com.google.gson.Gson
import java.util.*

class RandomSequenceFile private constructor(
    untilSix: List<Int>,
    untilNine: List<Int>
) : BoundedRandom {
    private val untilSixRandom = ArrayDeque(untilSix)
    private val untilNineRandom = ArrayDeque(untilNine)

    override fun nextInt(bound: Int): Int {
        return if (bound == 5) untilSixRandom.poll()
        else untilNineRandom.poll()
    }

    companion object {
        fun initialize(): RandomSequenceFile {
            val randomNumbers = Gson().fromJson(
                FileUtils.reader("random.json"),
                RandomNumFile::class.java
            )
            return RandomSequenceFile(
                randomNumbers.untilFive,
                randomNumbers.untilNine
            )
        }
    }
}

class RandomNumFile(val untilNine: List<Int>, val untilFive: List<Int>)
