package com.adaptionsoft.games.trivia.runner

import kotlin.streams.toList

class FileUtils {
    companion object {
        fun fileLines(fileName: String) = reader(fileName).use { it.lines().toList() }

        fun reader(fileName: String) = this::class.java
            .classLoader.getResourceAsStream(fileName)!!
            .bufferedReader()
    }
}
